// Motor Driver pins
int EN = 11;
int IN1 = 9;
int IN2 = 8;

// Ultrasonic Sensor pins
int trig = 12;
int echo = 13;

// Ultrasonic variables
long duration;
int distance;

void setup() {
  // Motor Driver setup
  pinMode(EN, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);

  // Ultrasonic setup
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);

  // Serial monitor
  Serial.begin(9600);
}

void loop() {
  // Ultrasonic getting distance in inches
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  duration = pulseIn(echo, HIGH);
  distance = duration*0.0133/2;

  // Setting DC Motor to rotate clockwise
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);

  // When an object is within 6 inches of the Ultrasonic
  if (distance < 6){
    // Stop the motor
    analogWrite(EN, 0);
    delay(1000);
  }
  else {
    // Start Motor
    analogWrite(EN, 255);
    delay(1000);
  }
}
